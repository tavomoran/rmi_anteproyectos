/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;


import java.rmi.RemoteException;
import sop_rmi.ClsServidorAnteproyectosImpl;
import servidor.utilidades.UtilidadesRegistroS;
public class ServidorDeObjetos
{
    public static void main(String args[]) throws RemoteException
    {         
        int numPuertoRMIRegistry = 2020;
        String direccionIpRMIRegistry = "localhost";                          
        System.out.println("Va a escuchar por: " + direccionIpRMIRegistry + numPuertoRMIRegistry);        
        
        try
        {
           ClsServidorAnteproyectosImpl objRemoto = new ClsServidorAnteproyectosImpl();
           UtilidadesRegistroS.arrancarNS(numPuertoRMIRegistry);
           UtilidadesRegistroS.RegistrarObjetoRemoto(objRemoto, direccionIpRMIRegistry, numPuertoRMIRegistry, "objAnteproyecto");            
           
        } catch (Exception e)
        {
            System.err.println("No fue posible Arrancar el NS o Registrar el objeto remoto" +  e.getMessage());
        }
        
        
    }
}