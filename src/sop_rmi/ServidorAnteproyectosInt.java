/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sop_rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import sop_rmi.dto.ClsAnteproyectoDTO;
import sop_rmi.dto.ClsEvaluadoresDTO;
import sop_rmi.dto.ClsUsuarioDTO;

/**
 *
 * @author acer
 */
public interface ServidorAnteproyectosInt extends Remote{
    public boolean login(ClsUsuarioDTO usuario) throws RemoteException;
    public boolean registrarUsuario(ClsUsuarioDTO usuario) throws RemoteException;
    public boolean registrarAnteproyecto(ClsAnteproyectoDTO anteproyecto) throws RemoteException;
    public boolean asignarEvaluadores(ClsEvaluadoresDTO evaluadores) throws RemoteException;
    public ClsAnteproyectoDTO consultarAnteproyecto(int codigo) throws RemoteException;
    public ArrayList<ClsAnteproyectoDTO> listarAnteproyectos() throws RemoteException;
    public boolean modificarConcepto() throws RemoteException;
    public boolean ingresarConcepto(int concepto) throws RemoteException;
    public ClsEvaluadoresDTO consultarEvaluadores(int codigo) throws RemoteException;
}
