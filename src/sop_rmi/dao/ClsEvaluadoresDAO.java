/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sop_rmi.dao;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Date;
import java.text.ParseException;
import sop_rmi.dto.ClsArchivoDTO;
import sop_rmi.dto.ClsEvaluadoresDTO;

/**
 *
 * @author acer
 */
public class ClsEvaluadoresDAO {
    private ClsArchivoDTO archivo;
    private ClsEvaluadoresDTO eval;
    
    public ClsEvaluadoresDAO() {
        
    }

    //crea un archivo con el nombre de usuario enla carpeta suuarios
    public ClsEvaluadoresDAO(ClsEvaluadoresDTO eval) throws IOException {
        String ruta = System.getProperty("user.dir") + "/src/sop_rmi/dao/evaluadores/" + String.valueOf(eval.getCodigo());
        archivo = new ClsArchivoDTO(ruta);
        this.eval = eval;
    }
    
    public ClsEvaluadoresDAO(int eval) throws IOException {
        String ruta = System.getProperty("user.dir") + "/src/sop_rmi/dao/evaluadores/" + String.valueOf(eval);
        archivo = new ClsArchivoDTO(ruta);
        this.eval = new ClsEvaluadoresDTO();
        this.eval.setCodigo(eval);
    }
    
    public boolean guardarEvaluadores() throws IOException {
        archivo.abrirArchivo(true);
        archivo.escribirArchivo(String.valueOf(eval.getCodigo()));
        archivo.escribirArchivo(eval.getNom_eval1());
        archivo.escribirArchivo(String.valueOf(eval.getCon_eval1()));
        archivo.escribirArchivo(String.valueOf(eval.getFecha_rev1()));
        archivo.escribirArchivo(eval.getNom_eval2());
        archivo.escribirArchivo(String.valueOf(eval.getCon_eval2()));
        archivo.escribirArchivo(String.valueOf(eval.getFecha_rev2()));
        archivo.cerrarArchivo();
        return true;
    }
    
    public boolean existe (String nombre){
        String ruta = System.getProperty("user.dir")+ "/src/sop_rmi/dao/evaluadores/" + nombre;
        return Files.exists(Paths.get(ruta));
    }
    
    public ClsEvaluadoresDTO consultarAnteproyecto(int codigo) throws IOException, ParseException
    {
        archivo.abrirArchivo(false);
        ClsEvaluadoresDTO consultado;
        String archi = archivo.leerArchivo();
        String[] info = archi.split(";");
        //leer archivo
        String cod = info[0];
        String eval1 = info[1];
        String concepto1 = info[2];
        String fecha1 = info[3];
        String eval2 = info[4];
        String concepto2 = info[5];
        String fecha2 = info[6];
                
        archivo.cerrarArchivo();
        int codi = Integer.parseInt(cod);
        int conc1 = Integer.parseInt(concepto1);
        int conc2 = Integer.parseInt(concepto2);
        
        //SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        
        Date f1 = Date.valueOf(fecha1);
        Date f2 = Date.valueOf(fecha2);
        
        //Date fregistro = format.parse(freg);
        //Date faprob = format.parse(fapro);
        
        //crear un objeto ClsEmpresaDTO
        consultado = new ClsEvaluadoresDTO(codi, eval1, conc1, f1, eval2, conc2, f2);
        //retornar el objeto
        
        return consultado;
    }
}
