/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sop_rmi.dao;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Date;
import java.text.ParseException;

import java.text.SimpleDateFormat;

import sop_rmi.dto.ClsAnteproyectoDTO;
import sop_rmi.dto.ClsArchivoDTO;

/**
 *
 * @author acer
 */
public class ClsAnteproyectoDAO {
    private ClsArchivoDTO archivo;
    private ClsAnteproyectoDTO antep;
    
    public ClsAnteproyectoDAO(){
        
    }

    //crea un archivo con el nombre de usuario enla carpeta suuarios
    public ClsAnteproyectoDAO(ClsAnteproyectoDTO antep) throws IOException {
        String ruta = System.getProperty("user.dir") + "/src/sop_rmi/dao/anteproyectos/" + String.valueOf(antep.getCodigo());
        archivo = new ClsArchivoDTO(ruta);
        this.antep = antep;
    }
    
    public ClsAnteproyectoDAO(int antep) throws IOException {
        String ruta = System.getProperty("user.dir") + "/src/sop_rmi/dao/anteproyectos/" + String.valueOf(antep);
        archivo = new ClsArchivoDTO(ruta);
        this.antep = new ClsAnteproyectoDTO();
        this.antep.setCodigo(antep);
    }
    
    public boolean guardarAnteproyecto() throws IOException {
        archivo.abrirArchivo(true);
        archivo.escribirArchivo(antep.getModalidad());
        archivo.escribirArchivo(antep.getTitulo());
        archivo.escribirArchivo(String.valueOf(antep.getCodigo()));
        archivo.escribirArchivo(antep.getNom_est1());
        archivo.escribirArchivo(antep.getNom_est2());
        archivo.escribirArchivo(antep.getNom_director());
        archivo.escribirArchivo(antep.getNom_codirector());
        archivo.escribirArchivo(String.valueOf(antep.getFecha_registro()));
        archivo.escribirArchivo(String.valueOf(antep.getFecha_aprobacion()));
        archivo.escribirArchivo(String.valueOf(antep.getConcepto()));
        archivo.escribirArchivo(String.valueOf(antep.getEstado()));
        archivo.escribirArchivo(String.valueOf(antep.getNum_revision()));
        archivo.cerrarArchivo();
        return true;
        
    }

    public boolean existe (String nombre){
        String ruta = System.getProperty("user.dir")+ "/src/sop_rmi/dao/anteproyectos/" + nombre;
        return Files.exists(Paths.get(ruta));
    }

    public ClsAnteproyectoDTO consultarAnteproyecto(int codigo) throws IOException, ParseException
    {
        archivo.abrirArchivo(false);
        ClsAnteproyectoDTO consultado;
        String archi = archivo.leerArchivo();
        String[] info = archi.split(";");
        //leer archivo
        String mod = info[0];
        String tit = info[1];
        String cod = info[2];
        String nest1 = info[3];
        String nest2 = info[4];
        String dir = info[5];
        String codir = info[6];
        String freg = info[7];
        String fapro = info[8];
        String con = info[9];
        String state = info[10];
        String nrev = info[11];
        
        System.out.println("fecha de registro: " + freg);
        System.out.println("fecha de aprobación: " + fapro);
                
        archivo.cerrarArchivo();
        int codi = Integer.parseInt(cod);
        int conc = Integer.parseInt(con);
        int estado = Integer.parseInt(state);
        int numrev = Integer.parseInt(nrev);
        
        //SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        
        Date fregistro = Date.valueOf(freg);
        Date faprob = Date.valueOf(fapro);
        
        //Date fregistro = format.parse(freg);
        //Date faprob = format.parse(fapro);
        
        //crear un objeto ClsEmpresaDTO
        consultado = new ClsAnteproyectoDTO(mod, tit, codi, nest1, nest2, dir, codir,fregistro, faprob, Integer.parseInt(con), Integer.parseInt(state), Integer.parseInt(nrev));
        //retornar el objeto
        
        return consultado;
    }
    
    public int nroAnteproyectos() {
        String ruta = System.getProperty("user.dir") + "/src/sop_rmi/dao/anteproyectos/";
        
        File Files = new File(ruta);
        return Files.list().length;
    }
    
}
