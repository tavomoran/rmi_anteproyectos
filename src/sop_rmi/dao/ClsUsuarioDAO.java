/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sop_rmi.dao;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import sop_rmi.dto.ClsArchivoDTO;
import sop_rmi.dto.ClsUsuarioDTO;

/**
 *
 * @author acer
 */
public class ClsUsuarioDAO {
    private ClsArchivoDTO archivo;
    private ClsUsuarioDTO usuario;
    
    public ClsUsuarioDAO() {
        
    }

    //crea un archivo con el nombre de usuario enla carpeta suuarios
    public ClsUsuarioDAO(ClsUsuarioDTO usuario) throws IOException {
        String ruta = System.getProperty("user.dir")+ "/src/sop_rmi/dao/usuarios/" + usuario.getUsuario();
        archivo = new ClsArchivoDTO(ruta);
        this.usuario = usuario;
    }
    
    public boolean guardarUsuario() throws IOException {
        archivo.abrirArchivo(true);
        archivo.escribirArchivo(usuario.getNombresyap());
        archivo.escribirArchivo(String.valueOf(usuario.getIdentificacion()));
        archivo.escribirArchivo(usuario.getUsuario());
        archivo.escribirArchivo(usuario.getContrasena());
        archivo.cerrarArchivo();
        return true;
        
    }
    
    public boolean existe (String nombre) {
        String ruta = System.getProperty("user.dir")+ "/src/sop_rmi/dao/usuarios/" + nombre;
        return Files.exists(Paths.get(ruta));
    }

    public ClsUsuarioDTO consultarUsuario(String usuario) throws IOException
    {
        archivo.abrirArchivo(false);
        ClsUsuarioDTO consultado;
        String archi = archivo.leerArchivo();
        String[] info = archi.split(";");
        //leer archivo
        String nom = info[0];
        String ide = info[1];
        String usu = info[2];
        String con = info[3];
        
        archivo.cerrarArchivo();
        int iden = Integer.parseInt(ide);
        //crear un objeto ClsEmpresaDTO
        consultado = new ClsUsuarioDTO(nom, iden, usu, con);
        //retornar el objeto
        
        return consultado;       
    }
    
}
