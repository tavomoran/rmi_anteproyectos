/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sop_rmi;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import sop_rmi.dao.ClsAnteproyectoDAO;
import sop_rmi.dao.ClsEvaluadoresDAO;
import sop_rmi.dao.ClsUsuarioDAO;
import sop_rmi.dto.ClsAnteproyectoDTO;
import sop_rmi.dto.ClsEvaluadoresDTO;
import sop_rmi.dto.ClsUsuarioDTO;

/**
 *
 * @author acer
 */
public class ClsServidorAnteproyectosImpl extends UnicastRemoteObject implements ServidorAnteproyectosInt{
    
    public static CallbackInt objRemoto;
    private ArrayList<ClsAnteproyectoDTO> listaAnteproyectos;
    private int codigoAnteproyecto;
    private ClsUsuarioDAO usuarios;
    private int nroAnteproyectos;

    public ClsServidorAnteproyectosImpl() throws RemoteException {
        super();
        this.listaAnteproyectos = new ArrayList<ClsAnteproyectoDTO>();
        this.codigoAnteproyecto = 0;
        nroAnteproyectos = 0;
    }
    
    
    
    @Override
    public boolean registrarUsuario(ClsUsuarioDTO usuario) {
        System.out.println("Registrando usuario...");
        boolean valido = false;    
        try {
            ClsUsuarioDAO nuevo = new ClsUsuarioDAO(usuario);
            valido = nuevo.guardarUsuario();
        } catch (IOException ex) {
            Logger.getLogger(ClsServidorAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return valido;
    }

    @Override
    public boolean registrarAnteproyecto(ClsAnteproyectoDTO antep) {
        System.out.println("Registrando anteproyecto...");
        boolean valido = false;    
        try {
            actualizarNroAnteproyectos();
            antep.setCodigo(nroAnteproyectos);
            ClsAnteproyectoDAO nuevo = new ClsAnteproyectoDAO(antep);
            valido = nuevo.guardarAnteproyecto();
        } catch (IOException ex) {
            Logger.getLogger(ClsServidorAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return valido;
    }

    @Override
    public boolean asignarEvaluadores(ClsEvaluadoresDTO evaluadores) {
        System.out.println("Asignando evaluadores...");
        boolean valido = false;    
        //if(consultarAnteproyecto(evaluadores.getCodigo()).getCodigo()>0){ //exista el anteproyecto
            try {
                ClsAnteproyectoDAO existe = new ClsAnteproyectoDAO();
                if (existe.existe(String.valueOf(evaluadores.getCodigo()))){
                    ClsEvaluadoresDAO nuevo = new ClsEvaluadoresDAO(evaluadores);
                    valido = nuevo.guardarEvaluadores();
                }
            } catch (IOException ex) {
                Logger.getLogger(ClsServidorAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        //} else {
        //    valido = false;
        //}
        return valido;
    }

    @Override
    public ClsAnteproyectoDTO consultarAnteproyecto(int codigo) {
        ClsAnteproyectoDTO consultado = new ClsAnteproyectoDTO();
        ClsAnteproyectoDAO existe = new ClsAnteproyectoDAO();
        try {
            if(existe.existe(String.valueOf(codigo))) {
                ClsAnteproyectoDAO consultar = new ClsAnteproyectoDAO(codigo);            
                consultado = consultar.consultarAnteproyecto(codigo);            
            }
        } catch (IOException | ParseException ex) {
            Logger.getLogger(ClsServidorAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return consultado;
    }

    @Override
    public ArrayList<ClsAnteproyectoDTO> listarAnteproyectos() {
        this.listaAnteproyectos = new ArrayList<ClsAnteproyectoDTO>();
        actualizarNroAnteproyectos();
        for(int i=0; i< nroAnteproyectos; i++) {
            ClsAnteproyectoDAO iterador;
            try {
                iterador = new ClsAnteproyectoDAO(i);
                ClsAnteproyectoDTO aux = new ClsAnteproyectoDTO();
                aux = iterador.consultarAnteproyecto(i);
                listaAnteproyectos.add(aux);
            } catch (IOException | ParseException ex) {
                Logger.getLogger(ClsServidorAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listaAnteproyectos;
    }

    @Override
    public boolean modificarConcepto() {
       return true;
    }

    @Override
    public boolean ingresarConcepto(int concepto) {
        
        return true;
    }

    @Override
    public boolean login(ClsUsuarioDTO usuario) {
        boolean ingreso = false;
        try {
            if(usuario.getUsuario().equals("admin") && usuario.getContrasena().equals("admin")) {
                ingreso = true;
            } else {
                //System.out.println("llamando ClsUsuarioDAO");
                System.out.println(usuario.getNombresyap());
                System.out.println(usuario.getIdentificacion());
                System.out.println(usuario.getUsuario());
                System.out.println(usuario.getContrasena());
                
                ClsUsuarioDAO existe = new ClsUsuarioDAO();
                if(existe.existe(usuario.getUsuario())){
                    ClsUsuarioDAO consultar = new ClsUsuarioDAO(usuario);
                    //System.out.println("Despues de el llamando ClsUsuarioDAO");
                    ClsUsuarioDTO consultado = consultar.consultarUsuario(usuario.getUsuario());
                    if(consultado.getUsuario().equals(usuario.getUsuario()) && consultado.getContrasena().equals(usuario.getContrasena())) {
                        ingreso = true;
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(ClsServidorAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ingreso;
    }
    
    public void actualizarNroAnteproyectos() {
        // nroAnteproyectos = cantidad de anteproyectos que hay en los archivos;
        ClsAnteproyectoDAO actualiza = new ClsAnteproyectoDAO();
        nroAnteproyectos = actualiza.nroAnteproyectos();
    }

    @Override
    public ClsEvaluadoresDTO consultarEvaluadores(int codigo) throws RemoteException {
        ClsEvaluadoresDTO consultado = new ClsEvaluadoresDTO();
        ClsEvaluadoresDAO existe = new ClsEvaluadoresDAO();
        try {
            if(existe.existe(String.valueOf(codigo))) {
                ClsEvaluadoresDAO consultar = new ClsEvaluadoresDAO(codigo);            
                consultado = consultar.consultarAnteproyecto(codigo);            
            }
        } catch (IOException | ParseException ex) {
            Logger.getLogger(ClsServidorAnteproyectosImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return consultado;
    }
}
