/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sop_rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import sop_rmi.dto.ClsEvaluadoresDTO;

/**
 *
 * @author acer
 */
public interface CallbackInt extends Remote{
    public void notificarEvaluadores(ArrayList<ClsEvaluadoresDTO> evaluadores) throws RemoteException;
}
