/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sop_rmi.dto;

import java.io.Serializable;


public class ClsUsuarioDTO implements Serializable{

    
    private String nombresyap;
    private int identificacion;
    private String usuario;
    private String contrasena;
    
    public ClsUsuarioDTO(String nombresyap, int identificacion, String usuario, String contrasena) {
        this.nombresyap = nombresyap;
        this.identificacion = identificacion;
        this.usuario = usuario;
        this.contrasena = contrasena;
    }

    public ClsUsuarioDTO() {
    }
    
    public String getNombresyap() {
        return nombresyap;
    }

    public void setNombresyap(String nombresyap) {
        this.nombresyap = nombresyap;
    }

    public int getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }
    
    
}
