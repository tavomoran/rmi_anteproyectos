/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sop_rmi.dto;

import java.io.Serializable;
import java.util.Date;


public class ClsEvaluadoresDTO implements Serializable{
    private int codigo;
    private String nom_eval1;
    private int con_eval1;
    private Date fecha_rev1;
    private String nom_eval2;
    private int con_eval2;
    private Date fecha_rev2;

    public ClsEvaluadoresDTO(int codigo, String nom_eval1, int con_eval1, Date fecha_rev1, String nom_eval2, int con_eval2, Date fecha_rev2) {
        this.codigo = codigo;
        this.nom_eval1 = nom_eval1;
        this.con_eval1 = con_eval1;
        this.fecha_rev1 = fecha_rev1;
        this.nom_eval2 = nom_eval2;
        this.con_eval2 = con_eval2;
        this.fecha_rev2 = fecha_rev2;
    }

    public ClsEvaluadoresDTO() {
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNom_eval1() {
        return nom_eval1;
    }

    public void setNom_eval1(String nom_eval1) {
        this.nom_eval1 = nom_eval1;
    }

    public int getCon_eval1() {
        return con_eval1;
    }

    public void setCon_eval1(int con_eval1) {
        this.con_eval1 = con_eval1;
    }

    public Date getFecha_rev1() {
        return fecha_rev1;
    }

    public void setFecha_rev1(Date fecha_rev1) {
        this.fecha_rev1 = fecha_rev1;
    }

    public String getNom_eval2() {
        return nom_eval2;
    }

    public void setNom_eval2(String nom_eval2) {
        this.nom_eval2 = nom_eval2;
    }

    public int getCon_eval2() {
        return con_eval2;
    }

    public void setCon_eval2(int con_eval2) {
        this.con_eval2 = con_eval2;
    }

    public Date getFecha_rev2() {
        return fecha_rev2;
    }

    public void setFecha_rev2(Date fecha_rev2) {
        this.fecha_rev2 = fecha_rev2;
    }
    
    
    
    
}
