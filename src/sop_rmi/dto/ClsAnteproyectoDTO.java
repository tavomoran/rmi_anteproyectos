/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sop_rmi.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author acer
 */
public class ClsAnteproyectoDTO implements Serializable{
    private String modalidad;
    private String titulo;
    private int codigo;
    private String nom_est1;
    private String nom_est2;
    private String nom_director;
    private String nom_codirector;
    private Date fecha_registro;
    private Date fecha_aprobacion;
    private int concepto;
    private int estado;
    private int num_revision;

    public ClsAnteproyectoDTO(String modalidad, String titulo, int codigo, String nom_est1, String nom_est2, String nom_director, String nom_codirector, Date fecha_registro, Date fecha_aprobacion, int concepto, int estado, int num_revision) {
        this.modalidad = modalidad;
        this.titulo = titulo;
        this.codigo = codigo;
        this.nom_est1 = nom_est1;
        this.nom_est2 = nom_est2;
        this.nom_director = nom_director;
        this.nom_codirector = nom_codirector;
        this.fecha_registro = fecha_registro;
        this.fecha_aprobacion = fecha_aprobacion;
        this.concepto = concepto;
        this.estado = estado;
        this.num_revision = num_revision;
    }

    public ClsAnteproyectoDTO() {
        this.modalidad = "";
        this.titulo = "";
        this.codigo = -1;
        this.nom_est1 = "";
        this.nom_est2 = "";
        this.nom_director = "";
        this.nom_codirector = "";
        //this.fecha_registro = fecha_registro;
        //this.fecha_aprobacion = fecha_aprobacion;
        this.concepto = -1;
        this.estado = -1;
        this.num_revision = -1;
    }
    

    public String getModalidad() {
        return modalidad;
    }

    public void setModalidad(String modalidad) {
        this.modalidad = modalidad;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNom_est1() {
        return nom_est1;
    }

    public void setNom_est1(String nom_est1) {
        this.nom_est1 = nom_est1;
    }

    public String getNom_est2() {
        return nom_est2;
    }

    public void setNom_est2(String nom_est2) {
        this.nom_est2 = nom_est2;
    }

    public String getNom_director() {
        return nom_director;
    }

    public void setNom_director(String nom_director) {
        this.nom_director = nom_director;
    }

    public String getNom_codirector() {
        return nom_codirector;
    }

    public void setNom_codirector(String nom_codirector) {
        this.nom_codirector = nom_codirector;
    }

    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

    public Date getFecha_aprobacion() {
        return fecha_aprobacion;
    }

    public void setFecha_aprobacion(Date fecha_aprobacion) {
        this.fecha_aprobacion = fecha_aprobacion;
    }

    public int getConcepto() {
        return concepto;
    }

    public void setConcepto(int concepto) {
        this.concepto = concepto;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int getNum_revision() {
        return num_revision;
    }

    public void setNum_revision(int num_revision) {
        this.num_revision = num_revision;
    }

    
    
}
