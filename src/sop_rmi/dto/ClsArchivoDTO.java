/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sop_rmi.dto;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author acer
 */
public class ClsArchivoDTO {
     
    private BufferedWriter archivoEscritura;
    private BufferedReader archivoLectura;
    private String ruta;
    
    public ClsArchivoDTO(String ruta) throws IOException {
        File archivo = new File(ruta);
        
        if(!archivo.exists())
        {
            this.archivoEscritura =  new BufferedWriter(new FileWriter(archivo));
            this.archivoLectura = new BufferedReader(new InputStreamReader(System.in));
        }
        this.ruta= ruta;
    }
    
    public ClsArchivoDTO() {
    }
    
    public void abrirArchivo(boolean escritura)
            throws IOException {
        if (escritura == true) {
            this.archivoEscritura = new BufferedWriter(new FileWriter(ruta, true));
        } else {
            System.out.println(ruta);
            this.archivoLectura = new BufferedReader(new FileReader(ruta));
        }
    }

    
    public void cerrarArchivo() throws IOException {
        if (archivoEscritura != null) {
            archivoEscritura.close();
        }
        if (archivoLectura != null) {
            archivoLectura.close();
        }
    }
    
    public boolean puedeLeer() throws IOException {
        return archivoLectura.ready();
    }

    public String[] LeerPalabras(int cantidad) {
        String[] palabras = new String[cantidad];
        int i = 0;
        try {
            while (this.puedeLeer() && i < cantidad) {
                palabras[i] = this.archivoLectura.readLine();
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return palabras;
    }
    
    public void escribirArchivo(String datos) throws IOException {
        archivoEscritura.write(datos +";");
    }
    
    public String leerArchivo() throws IOException {
        return archivoLectura.readLine();
    }
    
    public void GuardarArchivo() {
        try {
            File archivo = new File("C:\\Users\\carl9\\Desktop\\archivo\\Empresa.txt");
            if (archivo.createNewFile()) {
                System.out.println("Se creo un archivo de texto");
            }
        } catch (IOException e) {
            System.out.println("No se ha creado el archivo");
        }
    }
    
    public boolean existe(String nomArchivo) {
        boolean esta = false;
         // Aquí la carpeta donde queremos buscar
        String path = "/usuarios/";

        String files;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++)         {
            if (listOfFiles[i].isFile())             {
                files = listOfFiles[i].getName();
                if (files.equals(nomArchivo)) {
                    esta = true;
                }
            }
        }
        return esta;        
    }
    
    //Getters y setters

    public BufferedWriter getArchivoEscritura() {
        return archivoEscritura;
    }

    public void setArchivoEscritura(BufferedWriter archivoEscritura) {
        this.archivoEscritura = archivoEscritura;
    }

    public BufferedReader getArchivoLectura() {
        return archivoLectura;
    }

    public void setArchivoLectura(BufferedReader archivoLectura) {
        this.archivoLectura = archivoLectura;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }
    
    
    
}
